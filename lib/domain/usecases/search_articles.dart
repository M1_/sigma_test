import 'package:dartz/dartz.dart';
import 'package:sigma_test/common/failure.dart';
import 'package:sigma_test/domain/entities/articles.dart';
import 'package:sigma_test/domain/repositories/article_repository.dart';

class SearchArticles {
  final ArticleRepository repository;

  SearchArticles(this.repository);

  Future<Either<Failure, Articles>> execute(String query, {int page = 1}) {
    return repository.searchArticles(query, page: page);
  }
}
