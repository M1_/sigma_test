import 'package:dartz/dartz.dart';
import 'package:sigma_test/common/failure.dart';
import 'package:sigma_test/domain/entities/article.dart';
import 'package:sigma_test/domain/repositories/article_repository.dart';

class GetArticleCategory {
  final ArticleRepository repository;

  GetArticleCategory(this.repository);

  Future<Either<Failure, List<Article>>> execute(String category) {
    return repository.getArticleCategory(category);
  }
}
