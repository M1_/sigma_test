import 'package:dartz/dartz.dart';
import 'package:sigma_test/common/failure.dart';
import 'package:sigma_test/domain/entities/article.dart';
import 'package:sigma_test/domain/repositories/article_repository.dart';

class RemoveBookmarkArticle {
  final ArticleRepository repository;

  RemoveBookmarkArticle(this.repository);

  Future<Either<Failure, String>> execute(Article article) {
    return repository.removeBookmarkArticle(article);
  }
}
