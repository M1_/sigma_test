import 'package:dartz/dartz.dart';
import 'package:sigma_test/common/failure.dart';
import 'package:sigma_test/domain/entities/article.dart';
import 'package:sigma_test/domain/repositories/article_repository.dart';

class GetHeadlineBusinessArticles {
  final ArticleRepository repository;

  GetHeadlineBusinessArticles(this.repository);

  Future<Either<Failure, List<Article>>> execute() {
    return repository.getHeadlineBusinessArticles();
  }
}
